﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NekioriCoreSinAutf.Models;

namespace NekioriCoreSinAutf.Controllers
{
    public class CasoesController : Controller
    {
        private readonly NekioriContext _context;

        public CasoesController(NekioriContext context)
        {
            _context = context;
        }

        // GET: Casoes
        public async Task<IActionResult> Index()
        {
            var nekioriContext = _context.Caso.Include(c => c.IdClienteNavigation);
            return View(await nekioriContext.ToListAsync());
        }

        // GET: Casoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var caso = await _context.Caso
                .Include(c => c.IdClienteNavigation)
                .FirstOrDefaultAsync(m => m.IdCasos == id);
            if (caso == null)
            {
                return NotFound();
            }

            return View(caso);
        }

        // GET: Casoes/Create
        public IActionResult Create()
        {
            ViewData["IdCliente"] = new SelectList(_context.Cliente, "Id", "Contacto");
            return View();
        }

        // POST: Casoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdCasos,NroCaso,Estado,CantDatos,FechaIngreso,Listado,IdCliente")] Caso caso)
        {
            if (ModelState.IsValid)
            {
                _context.Add(caso);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Cliente, "Id", "Contacto", caso.IdCliente);
            return View(caso);
        }

        // GET: Casoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var caso = await _context.Caso.FindAsync(id);
            if (caso == null)
            {
                return NotFound();
            }
            ViewData["IdCliente"] = new SelectList(_context.Cliente, "Id", "Contacto", caso.IdCliente);
            return View(caso);
        }

        // POST: Casoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdCasos,NroCaso,Estado,CantDatos,FechaIngreso,Listado,IdCliente")] Caso caso)
        {
            if (id != caso.IdCasos)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(caso);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CasoExists(caso.IdCasos))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Cliente, "Id", "Contacto", caso.IdCliente);
            return View(caso);
        }

        // GET: Casoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var caso = await _context.Caso
                .Include(c => c.IdClienteNavigation)
                .FirstOrDefaultAsync(m => m.IdCasos == id);
            if (caso == null)
            {
                return NotFound();
            }

            return View(caso);
        }

        // POST: Casoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var caso = await _context.Caso.FindAsync(id);
            _context.Caso.Remove(caso);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CasoExists(int id)
        {
            return _context.Caso.Any(e => e.IdCasos == id);
        }
    }
}
