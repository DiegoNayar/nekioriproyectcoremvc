﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NekioriCoreSinAutf.Models;

namespace NekioriCoreSinAutf.Controllers
{
    public class DiscoesController : Controller
    {
        private readonly NekioriContext _context;

        public DiscoesController(NekioriContext context)
        {
            _context = context;
        }

        // GET: Discoes
        public async Task<IActionResult> Index()
        {
            var nekioriContext = _context.Disco.Include(d => d.IdCasoNavigation);
            return View(await nekioriContext.ToListAsync());
        }

        // GET: Discoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var disco = await _context.Disco
                .Include(d => d.IdCasoNavigation)
                .FirstOrDefaultAsync(m => m.IdDisco == id);
            if (disco == null)
            {
                return NotFound();
            }

            return View(disco);
        }

        // GET: Discoes/Create
        public IActionResult Create()
        {
            ViewData["IdCaso"] = new SelectList(_context.Caso, "IdCasos", "IdCasos");
            return View();
        }

        // POST: Discoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdDisco,Marca,Modelo,Capacidad,Serial,TipoDispositivo,Interfaz,IdCaso")] Disco disco)
        {
            if (ModelState.IsValid)
            {
                _context.Add(disco);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCaso"] = new SelectList(_context.Caso, "IdCasos", "IdCasos", disco.IdCaso);
            return View(disco);
        }

        // GET: Discoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var disco = await _context.Disco.FindAsync(id);
            if (disco == null)
            {
                return NotFound();
            }
            ViewData["IdCaso"] = new SelectList(_context.Caso, "IdCasos", "IdCasos", disco.IdCaso);
            return View(disco);
        }

        // POST: Discoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdDisco,Marca,Modelo,Capacidad,Serial,TipoDispositivo,Interfaz,IdCaso")] Disco disco)
        {
            if (id != disco.IdDisco)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(disco);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DiscoExists(disco.IdDisco))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCaso"] = new SelectList(_context.Caso, "IdCasos", "IdCasos", disco.IdCaso);
            return View(disco);
        }

        // GET: Discoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var disco = await _context.Disco
                .Include(d => d.IdCasoNavigation)
                .FirstOrDefaultAsync(m => m.IdDisco == id);
            if (disco == null)
            {
                return NotFound();
            }

            return View(disco);
        }

        // POST: Discoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var disco = await _context.Disco.FindAsync(id);
            _context.Disco.Remove(disco);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DiscoExists(int id)
        {
            return _context.Disco.Any(e => e.IdDisco == id);
        }
    }
}
