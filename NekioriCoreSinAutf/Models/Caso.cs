﻿using System;
using System.Collections.Generic;

namespace NekioriCoreSinAutf.Models
{
    public partial class Caso
    {
        public Caso()
        {
            Disco = new HashSet<Disco>();
        }

        public int IdCasos { get; set; }
        public int? NroCaso { get; set; }
        public string Estado { get; set; }
        public string CantDatos { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Listado { get; set; }
        public int? IdCliente { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual ICollection<Disco> Disco { get; set; }
    }
}
