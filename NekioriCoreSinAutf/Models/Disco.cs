﻿using System;
using System.Collections.Generic;

namespace NekioriCoreSinAutf.Models
{
    public partial class Disco
    {
        public int IdDisco { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Capacidad { get; set; }
        public string Serial { get; set; }
        public string TipoDispositivo { get; set; }
        public string Interfaz { get; set; }
        public int? IdCaso { get; set; }

        public virtual Caso IdCasoNavigation { get; set; }
    }
}
