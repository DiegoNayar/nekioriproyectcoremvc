﻿using System;
using System.Collections.Generic;

namespace NekioriCoreSinAutf.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Caso = new HashSet<Caso>();
        }

        public int Id { get; set; }
        public string Empresa { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Comuna { get; set; }
        public string Contacto { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Correo { get; set; }

        public virtual ICollection<Caso> Caso { get; set; }
    }
}
